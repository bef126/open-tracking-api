package main

import (
	"appengine/aetest"
	"appengine/datastore"
	"testing"
)

func TestUserStringer(t *testing.T) {
	u := User{
		Email:       "e",
		Password:    "p",
		DisplayName: "n",
		StringId:    "abc",
		Salt:        "salt",
	}
	result := u.String()
	expected := `{"Email":"e","Password":"p","DisplayName":"n","Salt":"salt","StringId":"abc"}`
	if result != expected {
		t.Errorf("result=%v expected=%v", result, expected)
	}
}

func TestUserExists(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const email = "nobody@example.com"

	// at first, no users should exist
	exists, err := userExists(email, c)
	if err != nil {
		t.Error(err.Error())
	}
	if exists {
		t.Error("user shouldn't exist")
	}

	// create a new user
	u := User{
		Email: email,
	}
	_, err2 := datastore.Put(c, newUserKey(c), &u)
	if err2 != nil {
		t.Error(err2.Error())
	}

	// new user should now exist
	exists, err = userExists(email, c)
	if err != nil {
		t.Error(err.Error())
	}
	if !exists {
		t.Error("user should exist")
	}
}

func TestCreateUser(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const email = "nobody@example.com"

	u, err := createUser(email, "p", "n", c)
	if err != nil {
		t.Error(err.Error())
	}

	if u.Email != email {
		t.Error("email doesn't match")
	}
	if u.DisplayName != "n" {
		t.Error("displayName doesn't match")
	}

	// if we try to create a user with the same email, we should get an error
	u, err = createUser(email, "p", "n", c)
	if err == nil {
		t.Error("duplicate emails should not be allowed")
	}
}

func TestRetrieveUser(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const email = "nobody@example.com"

	u, err := createUser(email, "p", "n", c)
	if err != nil {
		t.Error(err.Error())
	}

	u2, err2 := retrieveUser(email, c)
	if err2 != nil {
		t.Error(err2.Error())
	}

	if u.Email != u2.Email {
		t.Error("emails should match")
	}
	if u.StringId != u2.StringId {
		t.Error("ids should match")
	}
	if u.DisplayName != u2.DisplayName {
		t.Error("displayNames should match")
	}

	// try to retrieve a user that doesn't exist
	_, err3 := retrieveUser("iDontExist@example.com", c)
	if err3 == nil {
		t.Error("user should not exist")
	}
}

func TestHashPassword(t *testing.T) {
	const password = "abc123"
	pass := []byte(password)
	salt, err := MakeSalt()
	if err != nil {
		t.Error(err.Error())
	}
	h1 := HashPassword(pass, salt)
	h2 := HashPassword(pass, salt)
	if h1 != h2 {
		t.Error("hashes don't match")
	}
}

func TestVerifyPassword(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const email = "test@example.com"
	const password = "abc123"

	u, err := createUser(email, password, "test", c)
	if err != nil {
		t.Error(err)
	}

	yes := u.VerifyPassword(password)
	if yes == false {
		t.Error("password should be verified")
	}

	no := u.VerifyPassword("iAmNotThePassword")
	if no {
		t.Error("password should not be verified")
	}
}
