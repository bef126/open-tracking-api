package main

import (
	"appengine"
	"appengine/datastore"
	"code.google.com/p/go.crypto/pbkdf2"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
)

type User struct {
	Email       string
	Password    string
	DisplayName string
	Salt        string
	StringId    string `datastore:"-"`
}

// Stringer for User
func (u User) String() string {
	b, err := json.Marshal(u)
	if err != nil {
		fmt.Println("error:", err)
		return ""
	}
	return string(b[:])
}

// shared parent key so that every User is in the same entity group
func sharedUserKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "User", "default_user", 0, nil)
}

// unique key for each entity with a common parent key
func newUserKey(c appengine.Context) *datastore.Key {
	return datastore.NewIncompleteKey(c, "User", sharedUserKey(c))
}

func userExists(email string, c appengine.Context) (bool, error) {
	q := datastore.NewQuery("User").Ancestor(sharedUserKey(c)).Filter("Email =", email).KeysOnly()
	for t := q.Run(c); ; {
		var u User
		_, err := t.Next(&u)
		if err == datastore.Done {
			return false, nil
		}
		if err != nil {
			return false, err
		}
		return true, nil
	}
	return false, nil
}

func createUser(email, password, displayName string, c appengine.Context) (User, error) {
	exists, err := userExists(email, c)

	if err != nil {
		return User{}, err
	}

	if exists {
		err := errors.New("User Exists")
		return User{}, err
	} else {
		salt, se := MakeSalt()
		if se != nil {
			return User{}, se
		}
		passwordBytes := []byte(password)
		hashedPassword := HashPassword(passwordBytes, salt)
		u := User{
			Email:       email,
			Password:    hashedPassword,
			DisplayName: displayName,
			Salt:        ByteArrayToString(salt),
		}
		key, err := datastore.Put(c, newUserKey(c), &u)
		if err != nil {
			return User{}, err
		}

		// retrieve user we just inserted
		var u2 User
		if err = datastore.Get(c, key, &u2); err != nil {
			return User{}, err
		}

		u2.StringId = key.Encode()
		return u2, nil
	}
}

func retrieveUser(email string, c appengine.Context) (User, error) {
	q := datastore.NewQuery("User").Ancestor(sharedUserKey(c)).Filter("Email =", email)
	for t := q.Run(c); ; {
		var u User
		key, err := t.Next(&u)
		if err == datastore.Done {
			return User{}, errors.New("User Does Not Exist")
		}
		if err != nil {
			return User{}, err
		}
		u.StringId = key.Encode()
		return u, nil
	}
	return User{}, errors.New("User Does Not Exist")
}

func MakeSalt() ([]byte, error) {
	c := 10
	b := make([]byte, c)
	_, err := rand.Read(b)
	if err != nil {
		return b, err
	}
	return b, nil
}

func ByteArrayToString(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func StringToByteArray(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		b := make([]byte, 0)
		return b
	}
	return data
}

func HashPassword(password, salt []byte) string {
	byteArray := pbkdf2.Key(password, salt, 4096, sha256.Size, sha256.New)
	return ByteArrayToString(byteArray)
}

func (u User) VerifyPassword(input string) bool {
	salt := StringToByteArray(u.Salt)
	inputBytes := []byte(input)
	pwd := HashPassword(inputBytes, salt)
	return u.Password == pwd
}
