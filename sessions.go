package main

import (
	"appengine"
	"appengine/datastore"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

type Session struct {
	ExpDate  time.Time
	UserId   string
	StringId string `datastore:"-"`
}

// Stringer for Session
func (s Session) String() string {
	b, err := json.Marshal(s)
	if err != nil {
		fmt.Println("error:", err)
		return ""
	}
	return string(b[:])
}

// shared parent key so that every Session is in the same entity group
func sharedSessionKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Session", "default_session", 0, nil)
}

// unique key for each entity with a common parent key
func newSessionKey(c appengine.Context) *datastore.Key {
	return datastore.NewIncompleteKey(c, "Session", sharedSessionKey(c))
}

// create a new session
func createSession(u User, c appengine.Context) (Session, error) {
	// set expDate to 6 hours in the future
	expDate := time.Now().Add(SESSION_DURATION)
	s := Session{UserId: u.StringId, ExpDate: expDate}

	key, err := datastore.Put(c, newSessionKey(c), &s)
	if err != nil {
		return Session{}, err
	}

	s.StringId = key.Encode()
	return s, nil
}

// validate a session by it's token
// ensure it exists and is not exipred
// returns User mapped to session
func validateSession(token string, c appengine.Context) (User, error) {
	key, err := datastore.DecodeKey(token)
	if err != nil {
		return User{}, err
	}

	var s Session
	if err = datastore.Get(c, key, &s); err != nil {
		return User{}, err
	}

	// ensure session has not expired
	now := time.Now()
	if now.After(s.ExpDate) {
		return User{}, errors.New("Session Expired")
	}

	// retrieve User
	key, err2 := datastore.DecodeKey(s.UserId)
	if err2 != nil {
		return User{}, err2
	}
	var u User
	if err = datastore.Get(c, key, &u); err != nil {
		return User{}, err
	}

	u.StringId = key.Encode()
	return u, nil
}

// login and generate a new session
// login fails if email does not exist or if password doesn't match
func login(email, password string, c appengine.Context) (Session, error) {
	u, err := retrieveUser(email, c)
	if err != nil {
		return Session{}, err
	}

	if u.VerifyPassword(password) == false {
		err = errors.New("Invalid Login")
		return Session{}, err
	}

	s, err2 := createSession(u, c)
	if err2 != nil {
		return Session{}, err2
	}

	return s, nil

}
