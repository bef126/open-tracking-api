# Open Tracking API

Open Tracking is an open-source mobile tracking platform (currently Android-only). The platform consists of 3 main projects:

#### Open Tracking API

[https://bitbucket.org/bef126/open-tracking-api](https://bitbucket.org/bef126/open-tracking-api)

The Open Tracking API contains the API that the Android project uses and the tracking website for displaying the phone's location on a map. It is written in [Google Go](https://golang.org) to be run on [Google App Engine](https://cloud.google.com/appengine/docs).

#### Open Tracking Websocket

[https://bitbucket.org/bef126/open-tracking-websocket](https://bitbucket.org/bef126/open-tracking-websocket)

The Open Tracking Websocket server is a simple [socket.io](http://socket.io) server. It facilitates the real-time location updates between the mobile device and the tracking website. It was designed to run on a [Heroku](http://heroku.com) dyno, but can be easily modified to run on any [Node.js](http://nodejs.org) container.

#### Open Tracking Android

[https://bitbucket.org/bef126/open-tracking-android](https://bitbucket.org/bef126/open-tracking-android)

The Open Tracking Android project contains the mobile tracking app. The tracking app allows you to start and stop tracking, as well as send email invites for people to view the tracked location on a map.

## Setup

To run the API project, you first need to create a [Google App Engine](https://cloud.google.com/appengine/docs) identifier. To do this, go to your [App Engine Console](https://appengine.google.com/), click "Create Application", and fill out the form. Once you have one, simply copy it into the first line of the `app.yaml` file.

Next, you will need to install the [Google App Engine Go SDK](https://cloud.google.com/appengine/downloads#Google_App_Engine_SDK_for_Go) for your operating system.

## Testing

To run the test suite, run the following command:

`goapp test`

## Local Development Server

To run the development server, run the following command:

`goapp serve`

## Uploading

To upload to [Google App Engine](https://cloud.google.com/appengine/docs), run the following command:

`goapp deploy -oauth`

