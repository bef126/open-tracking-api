var OpenTracking = {};

OpenTracking.socket = null;
OpenTracking.marker = null;
OpenTracking.infoWindow = null;
OpenTracking.map = null;
OpenTracking.displayName = null;
OpenTracking.path = null;
OpenTracking.pathPoints = [];
OpenTracking.pathSize = 10;
OpenTracking.autoScroll = true;
OpenTracking.timer = null;
OpenTracking.me = null;

OpenTracking.bootstrap = function(mmap, channel, name, wsserver, pingChannel){
  OpenTracking.displayName = name;
  OpenTracking.map = mmap;
  
  OpenTracking.socket = io(wsserver);
  OpenTracking.socket.on('connect', function () {
    OpenTracking.socket.emit('join_channel', channel);
    // request a location ping so we can get the initial location of the pin
    OpenTracking.socket.emit('broadcast', {event:"ping", channel:pingChannel});
  });
  OpenTracking.socket.on('move', function(msg){
    console.log(msg);
    OpenTracking.move(msg);
  });

  // add click listener for auto-scroll
  $('#as').click(OpenTracking.toggleAutoScroll);

  // add click listener for show my location
  $('#ml').click(OpenTracking.toggleMyLocation);
}

OpenTracking.move = function(data){
  var myLatlng = new google.maps.LatLng(data.lat, data.lng);
  
  if(OpenTracking.marker){
    OpenTracking.marker.setPosition(myLatlng);
    if(OpenTracking.autoScroll){
      OpenTracking.map.panTo(myLatlng);
    }
    OpenTracking.updateInfoWindow(parseFloat(data.spd));
  } else {
    OpenTracking.createMarker(myLatlng);
  }
  
  OpenTracking.drawPath(myLatlng);
}

OpenTracking.updateInfoWindow = function(speed){
  var content = "<div style='height:45px;width:125px;text-align:center;'>"
  content += "<b>" + OpenTracking.displayName + "</b><br>";
  content += speed.toFixed(1);
  content += "</div>";
  OpenTracking.infoWindow.setContent(content);
}

OpenTracking.createMarker = function(ll){
  OpenTracking.marker = new google.maps.Marker({
    position: ll,
    map: OpenTracking.map,
    title: OpenTracking.displayName
  });
  OpenTracking.infoWindow = new google.maps.InfoWindow({
    content: "waiting for data . . ."
  });
  google.maps.event.addListener(OpenTracking.marker, 'click', function() {
    OpenTracking.infoWindow.open(OpenTracking.map, OpenTracking.marker);
  });
  OpenTracking.infoWindow.open(OpenTracking.map, OpenTracking.marker);
  OpenTracking.map.panTo(ll);
  OpenTracking.map.setZoom(14);
}

OpenTracking.addPointToPath = function(ll){
  if(OpenTracking.pathPoints.length >= OpenTracking.pathSize){
    var tempPath = [];
    for (index = 1; index < OpenTracking.pathPoints.length; index++) {
      tempPath[index-1] = OpenTracking.pathPoints[index];
    }
    OpenTracking.pathPoints = tempPath;
  }
  
  OpenTracking.pathPoints[OpenTracking.pathPoints.length] = ll;
}

OpenTracking.drawPath = function(ll){
  OpenTracking.addPointToPath(ll);
  
  // if previous line exists, remove it
  if(OpenTracking.path){
    OpenTracking.path.setMap(null);
  }
  
  OpenTracking.path = new google.maps.Polyline({
    path: OpenTracking.pathPoints,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  OpenTracking.path.setMap(OpenTracking.map);
}

OpenTracking.toggleAutoScroll = function(){
  OpenTracking.autoScroll = $('#as').is(':checked');
}

OpenTracking.toggleMyLocation = function(){
  if($('#ml').is(':checked')){
    OpenTracking.showMe();
    OpenTracking.timer = setInterval(OpenTracking.showMe, 10000);
  } else {
    clearInterval(OpenTracking.timer);
    OpenTracking.hideMe();
  }
}

OpenTracking.showMe = function(){
  if (navigator.geolocation){
    navigator.geolocation.getCurrentPosition(OpenTracking.locationSuccess, OpenTracking.locationError);
  } else {
    alert("Location not supported on this browser");
    clearInterval(OpenTracking.timer);
  }
}

OpenTracking.locationSuccess = function(pos){
  var ll = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
  if(OpenTracking.me){
    OpenTracking.me.setPosition(ll);
  } else {
    var pin = new google.maps.Marker({
      clickable: false,
      icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                        new google.maps.Size(22,22),
                                        new google.maps.Point(0,18),
                                        new google.maps.Point(11,11)),
      shadow: null,
      zIndex: 3,
      map: OpenTracking.map
    });
    OpenTracking.me = pin;
  }
}

OpenTracking.locationError = function(error){
  clearInterval(OpenTracking.timer);
  switch(error.code) {
    case error.PERMISSION_DENIED:
      alert("Unable to show your location since you denied location sharing.")
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Location information is unavailable.");
      break;
    case error.TIMEOUT:
      alert("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      alert("Cannot show your location at this time.");
      break;
  }
}

OpenTracking.hideMe = function(){
  if(OpenTracking.me){
    OpenTracking.me.setMap(null);
    OpenTracking.me = null;
  }
}
