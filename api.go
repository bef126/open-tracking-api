package main

import (
	"appengine"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// An input parameter for each API method
type Parameter struct {
	Name     string
	Required bool
}

func init() {
	http.HandleFunc("/v1/register", registerHandler)
	http.HandleFunc("/v1/login", loginHandler)
	http.HandleFunc("/v1/createInvite", createInviteHandler)
}

func validateParameters(params []Parameter, r *http.Request) error {
	for _, v := range params {
		if v.Required {
			if r.FormValue(v.Name) == "" {
				return errors.New(fmt.Sprintf("%v is required\n", v.Name))
			}
		}
	}
	return nil
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "email", Required: true},
		Parameter{Name: "password", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	email := r.FormValue("email")
	password := r.FormValue("password")

	u, err := createUser(email, password, "Open Tracking", c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// only return a subset of fields
	var m = map[string]string{
		"Email":       u.Email,
		"DisplayName": u.DisplayName,
		"UserId":      u.StringId,
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, mapToJson(m))
}

func mapToJson(m map[string]string) string {
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println("error:", err)
		return ""
	}
	return string(b[:])
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "email", Required: true},
		Parameter{Name: "password", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	email := r.FormValue("email")
	password := r.FormValue("password")

	s, err := login(email, password, c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// append websocket_server to response
	var m = map[string]string{
		"ExpDate":         s.ExpDate.Format(DATETIME_FORMAT),
		"WebsocketServer": WEBSOCKET_URL,
		"Token":           s.StringId,
		"UserId":          s.UserId,
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, mapToJson(m))
}

func createInviteHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	params := []Parameter{
		Parameter{Name: "t", Required: true},
	}
	paramsErr := validateParameters(params, r)
	if paramsErr != nil {
		http.Error(w, paramsErr.Error(), http.StatusInternalServerError)
		return
	}

	// validate session
	token := r.FormValue("t")
	u, err := validateSession(token, c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create new invite
	i, err2 := createInvite(u, c)
	if err2 != nil {
		http.Error(w, err2.Error(), http.StatusInternalServerError)
		return
	}

	var m = map[string]string{
		"ExpDate": i.ExpDate.Format(DATETIME_FORMAT),
		"Token":   i.StringId,
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, mapToJson(m))
}
