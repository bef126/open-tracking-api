package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"
	"testing"

	"appengine"

	"github.com/mzimmerman/appenginetesting"
)

func MockContext(t *testing.T) (*appenginetesting.Context, error) {
	c, err := appenginetesting.NewContext(&appenginetesting.Options{
		AppId:   "open-tracking",
		Testing: t,
		Debug:   appenginetesting.LogChild,
		Modules: []appenginetesting.ModuleConfig{
			{
				Name: "default",
				Path: filepath.Join("app.yaml"),
			},
		},
	})

	return c, err
}

func MockPost(path string, args url.Values, t *testing.T, c *appenginetesting.Context) (string, int, error) {
	defHost, err2 := appengine.ModuleHostname(c, "default", "", "")
	if err2 != nil {
		return "", 0, err2
	}

	resp, err3 := http.PostForm("http://"+defHost+path, args)
	if err3 != nil {
		return "", 0, err3
	}

	bodyBytes, err4 := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err4 != nil {
		return "", 0, err4
	}

	body := strings.TrimSpace(string(bodyBytes))

	return body, resp.StatusCode, nil
}

// parse JSON string into obj
func ParseJsonIntoStruct(j string, obj interface{}) error {
	dec := json.NewDecoder(strings.NewReader(j))
	err := dec.Decode(obj)
	return err
}

// create a single test function so we can share the same context
func TestApiWrapper(t *testing.T) {
	c, err := MockContext(t)
	if err != nil {
		t.Error(err)
	}
	defer c.Close()

	MyTestRegisterHandlerMissingRequiredArgs(t, c)
	MyTestRegisterHandlerUserExists(t, c)
	MyTestRegisterHandler(t, c)

	MyTestLoginHandlerUserDoesNotExist(t, c)
	MyTestLoginHandlerInvalidPassword(t, c)
	MyTestLoginHandler(t, c)

	MyTestCreateInviteHandlerInvalidSession(t, c)
	MyTestCreateInviteHandler(t, c)
}

func MyTestRegisterHandlerMissingRequiredArgs(t *testing.T, c *appenginetesting.Context) {
	values := make(url.Values)
	values.Set("foo", "bar")

	path := "/v1/register"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 500 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}

	if body != "email is required" {
		t.Error("invalid error response")
	}
}

func MyTestRegisterHandlerUserExists(t *testing.T, c *appenginetesting.Context) {
	const email = "test@example.com"

	// create a new user
	_, err := createUser(email, "abc123", "test", c)
	if err != nil {
		t.Error(err)
	}

	values := make(url.Values)
	values.Set("email", email)
	values.Set("password", "abc123")

	path := "/v1/register"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 500 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}

	if body != "User Exists" {
		t.Error("invalid error response")
	}
}

func MyTestRegisterHandler(t *testing.T, c *appenginetesting.Context) {
	const email = "test2@example.com"

	values := make(url.Values)
	values.Set("email", email)
	values.Set("password", "abc123")

	path := "/v1/register"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, body)
	}

	if statusCode != 200 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}

	u, err2 := retrieveUser(email, c)
	if err2 != nil {
		t.Error(err2)
	}
	if u.Email != email {
		t.Error("email doesn't match")
	}
}

func MyTestLoginHandlerUserDoesNotExist(t *testing.T, c *appenginetesting.Context) {
	values := make(url.Values)
	values.Set("email", "nobodyhere@example.com")
	values.Set("password", "abc123")

	path := "/v1/login"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 500 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}

	if body != "User Does Not Exist" {
		t.Error("invalid error response")
	}
}

func MyTestLoginHandlerInvalidPassword(t *testing.T, c *appenginetesting.Context) {
	const email = "testinvalidpassword@example.com"

	// create a new user
	_, err := createUser(email, "abc123", "test", c)
	if err != nil {
		t.Error(err)
	}

	values := make(url.Values)
	values.Set("email", email)
	values.Set("password", "wrongpassword")

	path := "/v1/login"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 500 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}

	if body != "Invalid Login" {
		t.Error("invalid error response")
	}
}

func MyTestLoginHandler(t *testing.T, c *appenginetesting.Context) {
	const email = "testloginhandler@example.com"
	const password = "abc123"

	// create a new user
	u, err := createUser(email, password, "test", c)
	if err != nil {
		t.Error(err)
	}

	values := make(url.Values)
	values.Set("email", email)
	values.Set("password", password)

	path := "/v1/login"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 200 {
		t.Errorf("statusCode was %d - expecting 200", statusCode)
	}

	type JsonResponse struct {
		ExpDate, WebsocketServer, Token, UserId string
	}
	var r JsonResponse

	err = ParseJsonIntoStruct(body, &r)
	if err != nil {
		t.Error(err)
	}

	if u.StringId != r.UserId {
		t.Error("userID doesn't match")
	}
}

func MyTestCreateInviteHandlerInvalidSession(t *testing.T, c *appenginetesting.Context) {
	values := make(url.Values)
	values.Set("t", "idonotexist")

	path := "/v1/createInvite"
	_, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 500 {
		t.Errorf("statusCode was %d - expecting 500", statusCode)
	}
}

func MyTestCreateInviteHandler(t *testing.T, c *appenginetesting.Context) {
	const email = "test1@example.com"
	const password = "abc123"

	// create a new user
	_, err := createUser(email, password, "test", c)
	if err != nil {
		t.Error(err)
	}

	values := make(url.Values)
	values.Set("email", email)
	values.Set("password", password)

	path := "/v1/login"
	body, statusCode, err := MockPost(path, values, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	type JsonResponse struct {
		ExpDate, WebsocketServer, Token, UserId string
	}
	var r JsonResponse

	err = ParseJsonIntoStruct(body, &r)
	if err != nil {
		t.Error(err)
	}

	values2 := make(url.Values)
	values2.Set("t", r.Token)

	path = "/v1/createInvite"
	body, statusCode, err = MockPost(path, values2, t, c)
	if err != nil {
		t.Errorf("Error fetching page - %s - %s", path, err.Error())
	}

	if statusCode != 200 {
		t.Errorf("statusCode was %d - expecting 200", statusCode)
	}
}
