package main

import (
	"appengine/aetest"
	"appengine/datastore"
	"testing"
	"time"
)

func TestCreateInvite(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	u, err := createUser("a", "b", "c", c)
	if err != nil {
		t.Error(err.Error())
	}

	i, err2 := createInvite(u, c)
	if err2 != nil {
		t.Error(err2.Error())
	}

	if i.StringId == "" {
		t.Error("StringId should not be empty")
	}
	if u.StringId != i.UserId {
		t.Error("UserId doesn't match")
	}
}

func TestRetrieveInvite(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	u, err := createUser("a", "b", "c", c)
	if err != nil {
		t.Error(err.Error())
	}

	i, err2 := createInvite(u, c)
	if err2 != nil {
		t.Error(err2.Error())
	}

	i2, err3 := retrieveInvite(i.StringId, c)
	if err3 != nil {
		t.Error(err3.Error())
	}

	if i.StringId != i2.StringId {
		t.Error("StringId should match")
	}
	if i.UserId != i2.UserId {
		t.Error("UserId should match")
	}

	// force the invite to expire
	expDate := time.Now().Add(-INVITE_DURATION)
	i.ExpDate = expDate
	key, err5 := datastore.DecodeKey(i.StringId)
	if err5 != nil {
		t.Error(err5)
	}
	_, err4 := datastore.Put(c, key, &i)
	if err4 != nil {
		t.Error(err4)
	}
	_, err4 = retrieveInvite(i.StringId, c)
	if err4.Error() != "Invite Expired" {
		t.Error("this should be expired")
	}

}
