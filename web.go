package main

import (
	"appengine"
	"fmt"
	"net/http"
)

func init() {
	http.HandleFunc("/track", trackHandler)
}

func trackHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	token := r.FormValue("t")

	i, err := retrieveInvite(token, c)
	if err != nil {
		errTemplate := `
<!DOCTYPE html>
<html>
<body>
<h3>The invite you are trying to retrieve doesn't exist or is expired.</h3>
</body>
</html>
`
		fmt.Fprint(w, errTemplate)
		return
	}

	template := `
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
      html { height: 100%% }
      body { height: 100%%; margin: 0; padding: 0 }
      #map-canvas { height: 100%%; z-index:1; }
      #toolbar {
        position:absolute;
        left:85px;
        top:5px;
        z-index:2;
        background-color:white;
        padding:5px;
      }
      @media (max-width:600px) {
        #toolbar {
          position:absolute;
          left:50px;
          top:5px;
          z-index:2;
          background-color:white;
          padding:5px;
        }
      }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//open-tracking-websocket.herokuapp.com/socket.io/socket.io.js"></script>
    <script src="/public/app.js"></script>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA57PLDsKN3Nq23KOZ3NGC7GxTKhk88wt4">
    </script>
    <script type="text/javascript">
      function initialize() {
        var channel = "%s";
        var name = "%s";
        var wsserver = "%s";
        var pingChannel = "%s";
        var mapOptions = {
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': 'United States of America'}, function (results, status) {
          map.fitBounds(results[0].geometry.viewport);
        });
        OpenTracking.bootstrap(map, channel, name, wsserver, pingChannel);
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body>
    <div id="toolbar">
      <input type="checkbox" id="as" checked> Auto Scroll <input type="checkbox" id="ml"> Show My Location
    </div>
    <div id="map-canvas"/>
  </body>
</html>
`
	fmt.Fprint(w, fmt.Sprintf(template, i.UserId, "Open Tracking", "//open-tracking-websocket.herokuapp.com", token))
}
