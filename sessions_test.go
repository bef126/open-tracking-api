package main

import (
	"appengine/aetest"
	"appengine/datastore"
	"testing"
	"time"
)

func TestSessionStringer(t *testing.T) {
	s := Session{StringId: "abc", UserId: "xyz"}
	result := s.String()
	expected := `{"ExpDate":"0001-01-01T00:00:00Z","UserId":"xyz","StringId":"abc"}`
	if result != expected {
		t.Errorf("result=%v expected=%v", result, expected)
	}
}

func TestCreateSession(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	u, err := createUser("a", "b", "c", c)
	if err != nil {
		t.Error(err.Error())
	}

	s, err2 := createSession(u, c)
	if err2 != nil {
		t.Error(err2.Error())
	}

	if s.StringId == "" {
		t.Error("StringId should not be empty")
	}
	if u.StringId != s.UserId {
		t.Error("UserId doesn't match")
	}
}

func TestValidateSession(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	u, err := createUser("a", "b", "c", c)
	if err != nil {
		t.Error(err.Error())
	}

	s, err2 := createSession(u, c)
	if err2 != nil {
		t.Error(err2.Error())
	}

	u2, result := validateSession(s.StringId, c)
	if result != nil {
		t.Error("this should be a valid session")
	}

	if u.StringId != u2.StringId {
		t.Error("StringId should match")
	}

	// force the session to expire (by substracting the SESSION_DURATION from the current timestamp)
	expDate := time.Now().Add(-SESSION_DURATION)
	s.ExpDate = expDate
	key, err3 := datastore.DecodeKey(s.StringId)
	if err3 != nil {
		t.Error(err3)
	}
	_, err4 := datastore.Put(c, key, &s)
	if err4 != nil {
		t.Error(err4)
	}
	_, result = validateSession(s.StringId, c)
	if result.Error() != "Session Expired" {
		t.Error("this should be an invalid session")
	}

}

func TestLogin(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	const email = "nobody@example.com"
	const password = "abc123"

	u, err := createUser(email, password, "testUser", c)
	if err != nil {
		t.Error(err.Error())
	}

	s, err2 := login(email, password, c)
	if err2 != nil {
		t.Error(err2.Error())
	}
	if u.StringId != s.UserId {
		t.Error("ids should match")
	}

	// test invalid email
	_, err3 := login("iDontExist@example.com", password, c)
	if err3 == nil {
		t.Error("login should be invalid")
	}

	// test invalid password
	_, err4 := login(email, "badPassword", c)
	if err4 == nil {
		t.Error("login should be invalid")
	}
}
