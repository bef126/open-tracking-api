package main

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"time"
)

type Invite struct {
	ExpDate  time.Time
	UserId   string
	StringId string `datastore:"-"`
}

// shared parent key so that every Invite is in the same entity group
func sharedInviteKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Invite", "default_invite", 0, nil)
}

// unique key for each entity with a common parent key
func newInviteKey(c appengine.Context) *datastore.Key {
	return datastore.NewIncompleteKey(c, "Invite", sharedInviteKey(c))
}

func createInvite(u User, c appengine.Context) (Invite, error) {
	expDate := time.Now().Add(INVITE_DURATION)
	i := Invite{UserId: u.StringId, ExpDate: expDate}

	key, err := datastore.Put(c, newInviteKey(c), &i)
	if err != nil {
		return Invite{}, err
	}

	i.StringId = key.Encode()
	return i, nil
}

func retrieveInvite(token string, c appengine.Context) (Invite, error) {
	key, err := datastore.DecodeKey(token)
	if err != nil {
		return Invite{}, err
	}

	var i Invite
	if err = datastore.Get(c, key, &i); err != nil {
		return Invite{}, err
	}

	// ensure invite has not expired
	now := time.Now()
	if now.After(i.ExpDate) {
		return Invite{}, errors.New("Invite Expired")
	}

	i.StringId = key.Encode()
	return i, nil
}
