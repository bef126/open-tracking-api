package main

import (
	"time"
)

// how long a session token is valid until
// currently set at 365 days
const SESSION_DURATION = time.Duration(365 * (time.Hour * 24))

// how long an invite will last
const INVITE_DURATION = time.Duration(24 * time.Hour)

// URL for socket.io server
const WEBSOCKET_URL = "https://open-tracking-websocket.herokuapp.com"

// Date/Time format
const DATETIME_FORMAT = "Jan 2, 2006 at 3:04pm (MST)"
